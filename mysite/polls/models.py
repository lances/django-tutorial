from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text
    
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

# After making the models, go to settings.py and insert 'polls.apps.PollsConfig'

# Every time you make changes to the models, you must run makemigrations appname
# Optional: you can run "python manage.py sqlmigrate polls 0001" to see what Django will run in SQL
# If you’re interested, you can also run python manage.py check; this checks for any problems in your project without making migrations or touching the database.