from django.contrib import admin

# Register your models here.
from .models import Question, Choice
from django.contrib.auth.models import User

admin.site.register(Question)
admin.site.register(Choice)